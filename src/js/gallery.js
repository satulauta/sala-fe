var gallery = {};

gallery.init = function() {

  gallery.galleryFiles = [];
  gallery.currentIndex = 0;

  var imageLinks = document.getElementsByClassName('imgLink');

  for (var i = 0; i < imageLinks.length; i++) {

    var link = imageLinks[i];
    const filename = link.dataset.filemime;
    if (link.dataset.filemime && (filename.startsWith('image/'))
                                || filename.startsWith("video/")
                                ) {
      gallery.addGalleryFile(link);
    }
  }

  var backLink = document.getElementById('settingsButton');
  if (!backLink)
    return;

  var galleryLink = document.createElement('a');
  galleryLink.title = 'Gallery mode.\nLeft/right arrow: previous/next\nUp/down arrow: skip 10 previous/next\nHome/End: first/last\nEsc: exit\nDelete: remove from gallery';
  galleryLink.id = 'galleryLink';
  galleryLink.innerText = lang.gallery;
  backLink.parentNode.insertBefore(galleryLink, backLink);

  backLink.parentNode.insertBefore(document.createTextNode(' '), backLink);

  var separator = document.createElement('span');
  separator.innerHTML = '/';
  backLink.parentNode.insertBefore(separator, backLink);

  backLink.parentNode.insertBefore(document.createTextNode(' '), backLink);

  var outerPanel;
  var innerPanel;

  galleryLink.onclick = function() {

    if (!gallery.galleryFiles.length) {
      alert('No images to see');
      return;
    }

    gallery.displayImage(gallery.currentIndex);
    gallery.viewingGallery = true;

  }

  document.body.addEventListener('keydown', function clicked(event) {

    if (!gallery.viewingGallery) {
      return;
    }

    switch (event.key) {

    case 'Escape': {
      preview.remove();
      gallery.viewingGallery = false;
      event.preventDefault();

      break;
    }

    case 'Home': {
      gallery.displayImage(0);
      event.preventDefault();
      break;
    }

    case 'End': {
      gallery.displayImage(gallery.galleryFiles.length - 1);
      event.preventDefault();
      break;
    }

    case 'ArrowDown': {
      gallery.displayImage(gallery.currentIndex + 10);
      event.preventDefault();
      break;
    }

    case 'ArrowUp': {
      gallery.displayImage(gallery.currentIndex - 10);
      event.preventDefault();
      break;
    }

    case 'ArrowLeft': {
      gallery.displayImage(gallery.currentIndex - 1);
      event.preventDefault();
      break;
    }

    case 'ArrowRight': {
      gallery.displayImage(gallery.currentIndex + 1);
      event.preventDefault();
      break;
    }

    case 'Delete': {
      gallery.galleryFiles.splice(gallery.currentIndex, 1);

      if (!gallery.galleryFiles.length) {
        outerPanel.remove();
        return;
      } else {
        gallery.displayImage(gallery.currentIndex);
      }
      event.preventDefault();

      break;
    }

    }

  });

};

gallery.displayImage = function(index) {

  if (index < 0) {
    index = gallery.galleryFiles.length - 1;
  } else if (index >= gallery.galleryFiles.length) {
    index = 0;
  }

  gallery.currentIndex = index;

  //gallery.galleryImage.src = gallery.galleryFiles[index];

  gallery.galleryFiles[index].scrollIntoView({
    behavior: 'auto',
    block: 'center',
    inline: 'center',
  });
  preview.show(null, gallery.galleryFiles[index])

  if (index > 0) {
    //gallery.previousImage.src = gallery.galleryFiles[index - 1];
  }

  if (index < gallery.galleryFiles.length - 1) {
    //gallery.nextImage.src = gallery.galleryFiles[index + 1];
  }

};

gallery.addGalleryFile = function(url) {

  if (gallery.galleryFiles.indexOf(url) === -1) {
    gallery.galleryFiles.push(url);
  }

};

if (!api.mobile) {
  gallery.init();
}

var thumbs = {};

thumbs.init = function () {


  thumbs.playableTypes = ['video/webm', 'audio/mpeg', 'video/mp4',
    'video/ogg', 'audio/ogg', 'audio/webm', 'audio/flac'
  ];

  thumbs.videoTypes = ['video/webm', 'video/mp4', 'video/ogg'];

  if (settings.get('centeredMediaDisplay')) {
    return;
  }

  thumbs.registerThumbClicks();
};

function setMediaMaxWidth(media) {
  const mediaStart = media.getBoundingClientRect().x
  media.style.maxWidth = `calc(100vw - ${mediaStart + 35}px`;
}

thumbs.registerThumbClicks = function () {
  document.getElementById("divThreads").addEventListener("click", function clicked(event) {

    if (event.which === 2 || event.ctrlKey) {
      return true;
    }
  
    const target = event.target.parentNode;
    if (!target || !target.classList.contains("imgLink")) return;

    const mime = target.dataset.filemime;

    if (!mime.startsWith("image/") && thumbs.playableTypes.indexOf(mime) === -1) return false;

    if (mime.indexOf('image/') > -1) {
      thumbs.expandImage(event, target, mime);
    } else {
      thumbs.setPlayer(target, mime);
    }

    event.preventDefault();
  });
}


function fitMedia(media) {

  const pos = media.getBoundingClientRect();
  console.log(pos);

  const goesOverTop = pos.top < 0;
  const goesOverBot = pos.bottom > window.innerHeight;

  const notFullyInView = goesOverTop || goesOverBot;
  console.log(notFullyInView);

  if (notFullyInView) {
    media.scrollIntoView({
      behavior: 'auto',
      block: 'center',
      inline: 'center'
    });
  }
}


thumbs.expandImage = function (mouseEvent, link, mime) {

  link.parentNode.classList.toggle('expandedCell');

  var inner = link.parentNode.parentNode.parentNode;
  if (inner.classList.contains('contentOverflow')) {
    inner = inner.parentNode;
  }

  var thumb = link.getElementsByTagName('img')[0];

  if (thumb.style.display === 'none' || thumb.style.opacity !== '') {
    link.getElementsByClassName('imgExpanded')[0].outerHTML = '';
    thumb.style.display = '';
    thumb.style.opacity = '';

    if (thumb.getBoundingClientRect().top < 0) {
      //thumb.parentNode.parentNode.parentNode.scrollIntoView(true);
      const postContainer = thumb.closest(".postCell") || thumb.closest(".innerOP");
      postContainer.scrollIntoView();
    }

    return false;
  }

  var expanded = link.getElementsByClassName('imgExpanded')[0];

  if (expanded) {
    thumb.style.display = 'none';
    expanded.outerHTML = '';
    return false;
  } else {
    var expandedSrc = link.href;

    if (thumb.src === expandedSrc && mime !== 'image/svg+xml') {
      return false;
    }

    expanded = document.createElement('img');
    expanded.setAttribute('src', expandedSrc);
    expanded.className = 'imgExpanded';
    expanded.style.display = 'none';


    const fileHeight = link.dataset.fileheight;
    const filewidth = link.dataset.filewidth;


    const rouglyEstimatedHeight = fileHeight * (window.outerHeight / filewidth);
    const fitImageIntoViewport = settings.get("fitPictures")
      && ((rouglyEstimatedHeight <= window.outerHeight * 1.5) || filewidth * 0.7 > fileHeight);

    if (fitImageIntoViewport) expanded.style.maxHeight = "135vh";

    function imgLoad() {
      setTimeout(() => {
        setMediaMaxWidth(expanded);
        if (fitImageIntoViewport) fitMedia(expanded);

      });
  
      if (thumb.style.opacity !== '') {
        thumb.style.display = 'none';
        expanded.style.display = '';
      }
    }

    thumb.style.opacity = '0.4';

    if (expanded.complete) {
      imgLoad();
    } else {
      expanded.addEventListener('load', imgLoad, {
        once: true,
      });
    }

    link.appendChild(expanded);

    setMediaMaxWidth(expanded);
    //const imgStart = expanded.getBoundingClientRect().x
    //expanded.style.maxWidth = `calc(100vw - ${imgStart + 35}px`;
  }

  return false;
};


thumbs.setPlayer = function (link, mime) {

  var path = link.href;
  var parent = link.parentNode;

  var src = document.createElement('source');
  src.setAttribute('src', link.href);
  src.setAttribute('type', mime);

  var isVideo = thumbs.videoTypes.indexOf(mime) > -1;
  var video = document.createElement(isVideo ? 'video' : 'audio');

  video.setAttribute('controls', true);
  video.loop = true;
  video.style.display = 'block';
  video.src = path;
  video.autoplay = true;
  video.volume = 0.5;

  
  if (isVideo) {
    video.style.width = link.dataset.filewidth;
    video.style.height = link.dataset.fileheight; 
  }
  setMediaMaxWidth(video);

  const fitVideoOnScreen = isVideo && settings.get('fitVideoPlayer');
  if (fitVideoOnScreen) video.style.maxHeight = "85vh";

  if (fitVideoOnScreen) {
    const f = fitMedia;
    let t = null;
    video.addEventListener("loadeddata", () => setTimeout(() => f(video), 30), {once: true});
    /*
    // to prevent annoying jump effect if video is loaded with long delay and user scrolled away
    t = setTimeout(() => {
      document.addEventListener("scroll", () => {
          video.removeEventListener("loadedmetadata", f)
      }, {once: true});
    }, 1000);*/
  }

  var videoContainer = document.createElement('span');
  videoContainer.classList.add("videoContainer");

  var hideLink = document.createElement('a');
  hideLink.innerHTML = 'Sulje';
  hideLink.style.cursor = 'pointer';
  //hideLink.style.color = 'black';
  hideLink.className = 'hideLink';
  hideLink.style.fontSize = '13px';



  var newThumbLink = document.createElement('a');
  newThumbLink.href = link.href;
  newThumbLink.className = 'imgLink';

  var newThumb = document.createElement('img');
  newThumbLink.appendChild(newThumb);
  newThumb.src = link.childNodes[0].src;

  videoContainer.appendChild(video);
  videoContainer.appendChild(hideLink);

  link.appendChild(videoContainer);

  setMediaMaxWidth(video);
  link.querySelector("img").classList.add("hidden");

  hideLink.onclick = function (e) {
    newThumbLink.style.display = 'block';
    video.style.display = 'none';
    video.className = '';
    hideLink.style.display = 'none';
    video.pause();

    videoContainer.innerHTML = ''
    videoContainer.parentNode.removeChild(videoContainer);
    link.querySelector("img").classList.remove("hidden");
    e.preventDefault();
    return;
  };
};


thumbs.getDimensions = function (width, height, thumb, divisor = 1) {
  const thumbSize = 175;

  if (width == null || height == null) {
    var icons = ['/audioGenericThumb.png', '/genericThumb.png']
    if (icons.indexOf(thumb) >= 0) {
      width = 60;
      height = 65;
    } else {
      width = 200;
      height = 200;
    }
  } else if (width > thumbSize || height > thumbSize) {
    const ratio = width / height;

    if (ratio > 1) {
      width = thumbSize;
      height = thumbSize / ratio;
    } else {
      width = thumbSize * ratio;
      height = thumbSize;
    }
  }

  return [Math.trunc(width / divisor), Math.trunc(height / divisor)];
};

thumbs.init();
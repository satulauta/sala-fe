class Seuraus
{
	constructor(lauta, lanka, otsikko, postaukseni = [], viimeksiLuettu = null)
	{

		if (typeof lauta !== "string") throw new Error(`Lauta ei ole merkkijono vaan '${typeof lauta}'`);
		if (!Number.isInteger(lanka)) throw new Error(`Lanka ei ole merkkijono vaan '${typeof lanka}'`);
		if (typeof otsikko !== "string") throw new Error(`otsikko ei ole merkkijono vaan '${typeof otsikko}'`);
		if (!Array.isArray(postaukseni)) throw new Error(`Postaukseni ei ole taulukko vaan '${typeof postaukseni}'`);
		if (viimeksiLuettu && !Number.isInteger(viimeksiLuettu)) throw new Error(`viimeksiLuettu tyyppiä '${typeof viimeksiLuettu}'`);

		this.lauta = lauta;
		this.lanka = lanka;
		this.otsikko = otsikko;
		this.postaukseni = postaukseni;
		this.viimeksiLuettu = viimeksiLuettu || postaukseni[0];

		// ei tallenneta localStorageen
		this.postausMäärä = 0;
		this.uudetPostaukset = 0;
		this.uudetVastaukset = [];
	}

	annaSäilytysuoto() {
		return {
			lauta: this.lauta,
			lanka: this.lanka,
			otsikko: this.otsikko,
			postaukseni: this.postaukseni,
			viimeksiLuettu: this.viimeksiLuettu,
		}
	}
}

const seururi = {};
seururi.langat = [];
seururi.localStorageAvain = "seuratut";

seururi.tallenna = function() {
	localStorage.setItem(seururi.localStorageAvain, JSON.stringify(seururi.langat.map((it) => it.annaSäilytysuoto())))
};

seururi.lataaLevyltä = function() {
	try {
		const seuraukset = JSON.parse(localStorage.getItem(seururi.localStorageAvain));
		seururi.langat = seuraukset.map((it) => new Seuraus(it.lauta, it.lanka, it.otsikko, it.postaukseni, it.viimeksiLuettu));
	} catch(e) {
		console.error("Seurausten lataus epäonnistui.")
		console.error(e);

		seururi.langat = [];
	}
};

seururi.lisää = function(lauta, lanka, otsikko, viimeksiLuettu, postaukseni = []) {

	if (!Number.isInteger(lanka)) throw new Error("Lanka ei ole kokonaisluku");
	if (!Number.isInteger(viimeksiLuettu)) throw new Error("viimeksiLuettu ei ole kokonaisluku");

	if (!Array.isArray(postaukseni)) throw new Error(`Postaukseni on '${typeof postaukseni}' (tahdotaan taulukko)`)

	const vanhaSeuraus = seururi.langat.find((it) => it.lauta === lauta && it.lanka === lanka);
	if (vanhaSeuraus) {
		vanhaSeuraus.postaukseni = [...new Set([...vanhaSeuraus.postaukseni ,...postaukseni])];
		vanhaSeuraus.viimeksiLuettu = viimeksiLuettu;
		seururi.tallenna();
		return vanhaSeuraus;
	}
	// lauta, lanka, otsikko, postaukseni = [], viimeksiLuettu = null
	const seuraus = new Seuraus(lauta, lanka, otsikko, postaukseni, viimeksiLuettu);
	seururi.langat.push(seuraus);
	seururi.tallenna();

	return seuraus;
};


seururi.hae = function(lauta, lanka) {
	if (!Number.isInteger(lanka)) throw new Error("Lanka ei ole kokonaisluku");
	return seururi.langat.find((it) => it.lauta === lauta && it.lanka === lanka);
}

seururi.poista = function(seuraus) {
	console.log(`Poistetaan lanka ${seuraus.lauta}/${seuraus.lanka} seurauksesta.`);
	seururi.langat = seururi.langat.filter((it) => it !== seuraus);
	seururi.tallenna();

	const OP = document.querySelector(`[id='${seuraus.lanka}'][data-boarduri='${seuraus.lauta}']`);
	if (OP) OP.querySelector(".followButton").innerText = "Seuraa";

	const seurausRivi = document.getElementById(`seuraus_${seuraus.lauta}_${seuraus.lanka}`);
	if (seurausRivi)  seurausRivi.parentNode.removeChild(seurausRivi);
}

seururi.haeLanka = function(seuraus) {

	const osoite = `/${seuraus.lauta}/res/${seuraus.lanka}.json`;

	fetch(osoite)
		.then((response) => {
			if (response.status === 404) {
				seururi.poista(seuraus);
				return null;
			}

			if (!response.ok) {
				return null;
			}
			return response.json();
		})
		.then((json) => {

			if (!json) return;

			seuraus.postausMäärä = json.posts.length;

			const uudetPostaukset = json.posts.filter((it) => {
				return it.postId > seuraus.viimeksiLuettu;
			});
			seuraus.uudetPostaukset = uudetPostaukset.length;

			seuraus.postaukseni.forEach((postaukseni) => {
				const vastaukset = uudetPostaukset.filter((it) => {
					return it.message.indexOf(`>>${postaukseni}`) >= 0
				}).map((it) => it.postId);
				seuraus.uudetVastaukset = [...new Set([...seuraus.uudetVastaukset ,...vastaukset])];

				seururi.päivitäNäytin();
			});
		}).catch((e) => {
			console.info("Langan haku epäonnistui");
		})
}

seururi.näytäNäytin = function() {
	seururi.näytin.sisältö.style.display = "flex";
	localStorage.setItem("näytin", true);
}

seururi.piilotaNäytin = function() {
	seururi.näytin.sisältö.style.display = "none";
	localStorage.removeItem("näytin");
}

seururi.luoNäytin = function() {
	seururi.näytin = document.createElement("div");
	seururi.näytin = document.createElement("div");
	seururi.näytin.id ="lankaNäytin";

	document.body.appendChild(seururi.näytin);

	seururi.näytin.yläpalkki = document.createElement("div");
	seururi.näytin.yläpalkki.id = "yläpalkki"
	//seururi.näytin.yläpalkki.innerText = "Lankaseurain";

	seururi.näytin.appendChild(seururi.näytin.yläpalkki);


	seururi.näytin.yläpalkki.otsikko = document.createElement("span");
	seururi.näytin.yläpalkki.otsikko.innerText = "Lankaseurain";
	seururi.näytin.yläpalkki.otsikko.setAttribute("title", "Vastattujen lankojan ja vastausten seuraaja");
	seururi.näytin.yläpalkki.appendChild(seururi.näytin.yläpalkki.otsikko);


	seururi.näytin.yläpalkki.sulje = document.createElement("span");
	seururi.näytin.yläpalkki.sulje.id = "sulje";
	seururi.näytin.yläpalkki.sulje.innerText = "✖";
	seururi.näytin.yläpalkki.setAttribute("title", "Poista käytöstä");

	seururi.näytin.yläpalkki.appendChild(seururi.näytin.yläpalkki.sulje);


	seururi.näytin.yläpalkki.uudetViestiKpl = document.createElement("span");
	seururi.näytin.yläpalkki.uudetViestiKpl.id = "uudetViestiKpl";
	seururi.näytin.yläpalkki.uudetViestiKpl.innerText = "";
	seururi.näytin.yläpalkki.uudetViestiKpl.classList.add("viestit-kpl")
	seururi.näytin.yläpalkki.uudetViestiKpl.setAttribute("title", "Vastausta seuratuissa");

	seururi.näytin.yläpalkki.appendChild(seururi.näytin.yläpalkki.uudetViestiKpl);


	seururi.näytin.yläpalkki.vastauksetMinulleKpl = document.createElement("span");
	seururi.näytin.yläpalkki.vastauksetMinulleKpl.id = "vastauksetMinulleKpl";
	seururi.näytin.yläpalkki.vastauksetMinulleKpl.innerText = "";
	seururi.näytin.yläpalkki.vastauksetMinulleKpl.classList.add("viestit-kpl")
	seururi.näytin.yläpalkki.vastauksetMinulleKpl.setAttribute("title", "Vastausta viesteihini");

	seururi.näytin.yläpalkki.appendChild(seururi.näytin.yläpalkki.vastauksetMinulleKpl);


	seururi.näytin.sisältö = document.createElement("div");
	seururi.näytin.sisältö.id = "sisältö";

	seururi.näytin.appendChild(seururi.näytin.sisältö);


	seururi.näytin.sisältö.lankalista = document.createElement("div");
	seururi.näytin.sisältö.lankalista.id = "lankalista"

	seururi.näytin.sisältö.appendChild(seururi.näytin.sisältö.lankalista);


	seururi.näytin.sisältö.langaton = document.createElement("div");
	seururi.näytin.sisältö.langaton.setAttribute("id", "langaton");
	seururi.näytin.sisältö.langaton.innerText = "Ei seurattuja lankoja";

	/*
	seururi.näytin.sisältö.langaton.style.textAlign = "center";
	seururi.näytin.sisältö.langaton.style.color = "#242222";
	*/
	seururi.näytin.sisältö.appendChild(seururi.näytin.sisältö.langaton);

	seururi.näytin.yläpalkki.sulje.addEventListener("click", function(e) {
		settings.set("seurain", false);
		seururi.näytin.style.display = "none";
		const asetusValinta = document.getElementById("checkbox-seurain");
		if (asetusValinta) asetusValinta.checked = false;
	});


	seururi.näytin.yläpalkki.addEventListener("click", function(e) {
		if (seururi.näytin.sisältö.style.display !== "flex") seururi.näytäNäytin();
		else seururi.piilotaNäytin();
	});

	seururi.näytin.sisältö.lankalista.addEventListener("click", function(e) {
		if (!e.target.classList.contains("poisto")) return;

		const rivi = e.target.parentNode.parentNode;

		const lauta = rivi.dataset.lauta;
		const lanka = parseInt(rivi.dataset.lanka);

		rivi.parentNode.removeChild(rivi);

		const seuraus = seururi.hae(lauta, lanka);
		if (!seuraus) throw new Error(`Ei seurausta '${lauta}/${lanka}'`);
		seururi.poista(seuraus);
		//seururi.päivitäNäytin();
	})
	
}


seururi.päivitäNäytin = function() {

	const näkytila = (seururi.langat.length) ? "none" : "block";
	seururi.näytin.sisältö.langaton.style.display = näkytila;

	const reducer = (acc, it) => acc + it.uudetVastaukset.length;
	const vastaksetMinulleKpl = seururi.langat.reduce(reducer, 0) 
	seururi.näytin.yläpalkki.vastauksetMinulleKpl.innerText = vastaksetMinulleKpl;

	const reducer2 = (acc, it) => acc + it.uudetPostaukset;
	const uudetViestitLangoissaKpl =  seururi.langat.reduce(reducer2, 0)
	seururi.näytin.yläpalkki.uudetViestiKpl.innerText = uudetViestitLangoissaKpl;

	seururi.langat.forEach((seuraus) => {
		const id = `seuraus_${seuraus.lauta}_${seuraus.lanka}`;

		let rivi = seururi.näytin.sisältö.lankalista.querySelector(`#${id}`);
		if (!rivi) {
			const ekaLukematon = (seuraus.uudetPostaukset.length) ? ('#' + seuraus.uudetPostaukset[0]): '';
			const rivinPohja = `
			<div id='${id}' class='seurattu-lanka' style='display: flex;;' data-lauta='${seuraus.lauta}' data-lanka='${seuraus.lanka}'>
				<div class="viestit" style="flex: 1; display: flex;">
					<span class='vastaukset' style='margin-right: 5px' title='Uutta vastausta'></span>
					<span class='lukemattomat' title='Uutta viestiä seuratuissa'></span>
				</div>
				<div class='oikeat' style='display: flex; width: 300px; justify-content: flex-end'>
					<span class='lanka' 
						style='; margin: 0 2px 0 2px; display: inline-block; min-width: 100px; text-align: right'>
						<a class='linkki' style="display: block; overflow: hidden;  text-overflow: ellipsis"
						 href='/${seuraus.lauta}/res/${seuraus.lanka}.html${(ekaLukematon)}'></a>
					</span>
					<span style='cursor: pointer; color: #881c1c; margin-left: 3px' class='poisto' title='Poista'>✖</span>
				</div>
			<div>`.trim();
			const frag = document.createRange().createContextualFragment(rivinPohja);
			seururi.näytin.sisältö.lankalista.appendChild(frag);
			rivi = seururi.näytin.sisältö.lankalista.querySelector(`#${id}`);
		}
		
		rivi.querySelector(".vastaukset").innerText = seuraus.uudetVastaukset.length || ' ';
		rivi.querySelector(".lukemattomat").innerText = seuraus.uudetPostaukset || ' ';
		rivi.querySelector(".linkki").innerText = seuraus.otsikko.substr(0, 64);
		rivi.classList.remove("käsittelemätön");
	});
}

seururi.tarkastaSeuratut = function() {
	seururi.langat.forEach((seuraus) => {
		this.haeLanka(seuraus);
	});
}

seururi.init = function() {

	window.addEventListener("storage", function (e) {
		if (e && e.key === seururi.localStorageAvain) {
			seururi.lataaLevyltä();
		}
	});

	seururi.lataaLevyltä();

	const käytössä = settings.get("seurain");
	if (käytössä) {
		seururi.luoNäytin();
	}

	const lauta = api.boardUri;
	const lanka = api.threadId;

	if (lauta && lanka) {
		function asetaUusinLuetuksi() {
			const vanhaSeuraus = seururi.langat.find((it) => it.lauta === lauta && it.lanka === lanka);
			if (vanhaSeuraus) {
				const uusinPostaus = document.querySelector(".postCell:last-child");
				if (uusinPostaus) {
					vanhaSeuraus.viimeksiLuettu = parseInt(uusinPostaus.getAttribute("id"));
					seururi.tallenna();
				}
			}
		}
		asetaUusinLuetuksi();
		posting.eventEmitter.addListener("postAdded", asetaUusinLuetuksi);
		document.addEventListener("focus", asetaUusinLuetuksi);
	}

	postCommon.eventEmitter.addListener("postCreated", function postAdded(args) {
		const threadNo = args.thread || args.postNo;

		let otsikko = '';
		if (args.thread) {
			otsikko = document.title;
		} else {
			const subjectInput = document.getElementById("fieldSubject").value.trim().substr(0, 16);
			const messageInput = document.getElementById("fieldMessage").value.trim().substr(0, 16);
	
			otsikko = "/" + args.board + "/ - " + ((subjectInput.length) ? subjectInput : messageInput);
		}
	
		seururi.lisää(args.board, threadNo, otsikko, args.postNo, [args.postNo]);
		seururi.päivitäNäytin();
	});

	function käsitteleSeurantalinkit() {
		const seurausLinkit = document.getElementsByClassName("followButton");
		for (let i = 0; i < seurausLinkit.length; i++) {
			const linkki = seurausLinkit[i];

			if (!käytössä) {
				linkki.style.display = "none";
				continue;
			}

			const OP = linkki.closest(".opCell");
			const langanLauta = OP.dataset.boarduri;
			const lankaNo = parseInt(OP.id)

			const joSeurattu = !!seururi.langat.find((it) => it.lauta === langanLauta && it.lanka === lankaNo);

			if (joSeurattu) {
				linkki.innerText = "Poista seurannasta"
			}
			
			linkki.addEventListener("click", function klikattu(e) {

				const vanhaSeuraus = seururi.langat.find((it) => it.lauta === langanLauta && it.lanka === lankaNo);
				if (vanhaSeuraus) {
					seururi.poista(vanhaSeuraus);
					e.target.innerText = "Seuraa";
				} else {		
					let postaukseniLangassa = [];

					try {
						const kaikkiPostaukseni = JSON.parse(localStorage.getItem("postingPasswords"));
						const avaimet = Object.keys(kaikkiPostaukseni);
						const langanOsoite = `${langanLauta}/${OP.id}/`;
						avaimet.forEach((it) => {
							if (it.startsWith(langanOsoite)) {
								// "b/13/77" => 77
								const postNo  = parseInt(it.replace(langanOsoite, ""));
								postaukseniLangassa.push(postNo);
							}
						})
					} catch (e) {
						console.error(e);
					}

					const vanhinPostaus = OP.querySelector(".postCell:last-child");
					const vanhinPostausNo = (vanhinPostaus) ? parseInt(vanhinPostaus.id) : lankaNo

					const langanOtsikko = OP.querySelector(".labelSubject");
					const langanViesti = OP.querySelector(".divMessage").innerText;

					const otsikko = "/" + langanLauta + "/ - " + ((langanOtsikko && langanOtsikko.length) ? langanOtsikko : langanViesti);

					// lauta, lanka, otsikko, postausNo, postaukseni
					seururi.lisää(langanLauta, lankaNo, otsikko, vanhinPostausNo, postaukseniLangassa);
					seururi.päivitäNäytin();

					e.target.innerText = "Poista seurannasta";
				}

			});
		};
	}
	käsitteleSeurantalinkit();

	if (!lanka) {
		if (localStorage.getItem("näytin")) seururi.näytäNäytin();
		seururi.tarkastaSeuratut();
		//if (seururi.intervalli) clearInterval(seururi.intervalli);
		//seururi.intervalli = setInterval(() => seururi.tarkastaSeuratut(), 30 * 1000)
	} else {
		seururi.näytin.style.display = "none";
	}
};

seururi.init();

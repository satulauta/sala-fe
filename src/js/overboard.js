api.isBoard = true;

function insertBefore(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode);
}

function hideThreadsFromBoard(event) {
  var board = event.originalTarget.getAttribute("data-boarduri")
  toggle_board(board)
}

function hide_threads_from_board(board) {
  hide_these_threads = document.querySelectorAll('div.opCell[data-boarduri="' + board + '"]');  
  for(var i = 0; i < hide_these_threads.length; i++) {    
    hide_these_threads[i].getElementsByClassName("divPosts")[0].style.display = "none"
    hide_these_threads[i].getElementsByClassName("innerOP")[0].style.display = "none"
    //hide_these_threads[i].getElementsByClassName("unimportantHide")[0].textContent = "(Fäden dieses Brett's einblenden)"
  }
}

function show_threads_from_board(board) {
  hide_these_threads = document.querySelectorAll('div.opCell[data-boarduri="' + board + '"]');  
  for(var i = 0; i < hide_these_threads.length; i++) {    
    hide_these_threads[i].getElementsByClassName("divPosts")[0].style.display = ""
    hide_these_threads[i].getElementsByClassName("innerOP")[0].style.display = ""
   // hide_these_threads[i].getElementsByClassName("unimportantHide")[0].textContent = "(Fäden dieses Brett's ausblenden)"
  }
}

function toggle_board(board) {
  current_storage = localStorage.getItem("overboard_hidden_boards")
  parsed = JSON.parse(current_storage || "[]")
  if (parsed.indexOf(board) == -1 ) {
    parsed.push(board)
    localStorage.setItem("overboard_hidden_boards", JSON.stringify(parsed))
    hide_threads_from_board(board)
  } else {
    parsed.splice(parsed.indexOf(board), 1);
    localStorage.setItem("overboard_hidden_boards", JSON.stringify(parsed))
    show_threads_from_board(board)
  }
}

function hide_boards_from_localstorage() {
  current_storage = localStorage.getItem("overboard_hidden_boards")
  parsed = JSON.parse(current_storage || "[]")
  for(var i = 0; i < parsed.length; i++) { 
    hide_threads_from_board(parsed[i])
  }
}

var threadClasses = document.querySelectorAll("div.opCell");
for(var i = 0; i < threadClasses.length; i++) {
  var boarduri = threadClasses[i].getAttribute("data-boarduri");

  var holderDiv = document.createElement("div");
  holderDiv.style.marginLeft = "25px";


  var boardLink = document.createElement('a');
  boardLink.setAttribute('href', '/'+boarduri);
  boardLink.innerText = "/"+boarduri+"/";
  holderDiv.appendChild(boardLink);

  var hide_link = document.createElement("span");
  hide_link.innerText = "Lautapiilotus"
  hide_link.dataset.boarduri = boarduri;
  hide_link.style.marginLeft = "3px";
  hide_link.style.cursor = "pointer";

  hide_link.addEventListener("click", hideThreadsFromBoard);
  holderDiv.appendChild(hide_link);

      
  firstChild = threadClasses[i].children[1]
  //insertBefore(p, firstChild);
  insertBefore(holderDiv, firstChild);
  
}

hide_boards_from_localstorage();

var settings = {};

settings.modes = {
  /*
  'convertLocalTimes': {
    text: lang.localTimes,
    default: true
  },*/
/*
  'directDownload': {
    text: lang.directDownload,
    default: true
  },*/
  'qrMode': {
    text: lang.quickReply,
    default: true
  },
  'seurain': {
    text: "Lanka -ja vastausseuraaja",
    default: true
  },
  /*
  'centeredMediaDisplay': {
    text: "Keskitetty tiedostokatselin",
    default: false
  },*/
  "audioThread": {
    text: "Äänisoitin",
    default: true,
  },
  "fitVideoPlayer": {
    text: "Sovita laajennettu video ruudulle",
    default: true,
  },
  "fitPictures": {
    text: "Sovita laajennettu kuva ruudulle",
    default: false,
  },
  'sfwMode': {
    text: lang.sfwMode,
    default: false
  },
  'autoRefreshMode': {
    text: lang.autoReload,
    default: true
  },
  'showYous': {
    text: lang.showYous,
    default: false
  },
  'fixedTopNav': {
    text: lang.fixedTopNav,
    default: false
  },/*
  'relativeTime': {
    text: lang.relativeTimes,
    default: false
  },*/
  'scrollDownMode': {
    text: lang.scrollDownAfterPost,
    default: true
  },
  'scrollPostFormMode': {
    text: lang.scrollToPostformAfterQuote,
    default: true
  },
  'postCounter': {
    text: lang.postCounter,
    default: false
  },
  'autoMarkAsDeleted': {
    text: lang.autoMarkAsDeleted,
    default: true
  },
  'checkFileIdentifier': {
    text: lang.checkFileIdentifier,
    default: true
  },
  'ensureBypass': {
    text: lang.alwaysUseBypass,
    default: false
  },
};

settings.get = function(key) {

  if (localStorage[key] !== undefined) {

    return JSON.parse(localStorage[key]);

  } else {
    if (!settings.modes[key]) return null;

    return settings.modes[key].default;

  }

};

settings.set = function(key, value) {

  localStorage.setItem(key, value);

}

var expandComment = {};

expandComment.init = function() {

  expandComment.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

  if (expandComment.isSafari) {
    return;
  }

  setTimeout(() => {
    expandComment.applyAll();
  }, 50);
  window.onresize = function() {
    expandComment.applyAll(false);
  }

  posting.eventEmitter.addListener("postAdded", function postAdded(cell) {
    const contentOverflow = cell.querySelector(".contentOverflow");
    expandComment.apply(contentOverflow);
  });
  
};

expandComment.applyAll = function() {

  var messages = document.getElementsByClassName('contentOverflow');

  for (var i = 0; i < messages.length; i++) {
    expandComment.apply(messages[i]);
  }
}


expandComment.apply = function(contentOverflow) {

  const commentExpand = contentOverflow.nextElementSibling;
  if (!commentExpand) return;
  const divMessage = contentOverflow.querySelector(".divMessage");

  if (divMessage.scrollHeight - 5 > divMessage.clientHeight) {
    
    divMessage.scrollTop = 0;

    const percentage =  Math.round((divMessage.scrollHeight / divMessage.clientHeight) * 100) - 100;

    commentExpand.innerText = lang.commentExpand.replace('__percentage__', percentage);
    commentExpand.onclick = expandComment.expand;
    commentExpand.classList.remove("hidden");
  } else {
    commentExpand.classList.add("hidden");
  }

};

expandComment.expand = function() {

  const message = this.parentElement.querySelector(".divMessage");

  message.style.maxHeight = "40000px";
  this.classList.add("hidden")
};

expandComment.init();

var stats = {};

stats.init = function() {

  stats.refreshURL = "/index.json"

  stats.startTimer();

  var divLatestImages = document.getElementById('divLatestImages');
  var divLatestPosts = document.getElementById('divLatestPosts');
  var latestImagesHeading = document.getElementById('latestImagesHeading');
  var latestPostsHeading = document.getElementById('latestPostsHeading');
  stats.latestImagesReady = true;
  stats.latestPostsReady = true;

  divLatestImages.onmouseenter = function() {
    stats.latestImagesReady = false;
    latestImagesHeading.innerText = lang.latestImages + ' (' + lang.frozen + ')';
  };

  divLatestImages.onmouseleave = function() {
    stats.latestImagesReady = true;
    latestImagesHeading.innerText = lang.latestImages;
  };

  divLatestPosts.onmouseenter = function() {
    stats.latestPostsReady = false;
    latestPostsHeading.innerText = lang.latestPosts + ' (' + lang.frozen + ')';
  };

  divLatestPosts.onmouseleave = function() {
    stats.latestPostsReady = true;
    latestPostsHeading.innerText = lang.latestPosts;
  };

};

stats.refreshCallback = function(error, receivedData) {

  if (error) {
    return;
  }

  receivedData = JSON.parse(receivedData);

  var labelTotalPosts = document.getElementById('labelTotalPosts');
  var labelTotalPPH = document.getElementById('labelTotalPPH');
  var labelTotalFiles = document.getElementById('labelTotalFiles');
  var labelTotalSize = document.getElementById('labelTotalSize');
  var divLatestImages = document.getElementById('divLatestImages');
  var divLatestPosts = document.getElementById('divLatestPosts');

  if (labelTotalPosts)
    labelTotalPosts.innerText = receivedData.totalPosts;

  if (labelTotalPPH)
    labelTotalPPH.innerText = receivedData.totalPPH;

  if (labelTotalFiles)
    labelTotalFiles.innerText = receivedData.totalFiles;

  if (labelTotalSize)
    labelTotalSize.innerText = receivedData.totalSize;

  var latestImages = receivedData.latestImages;

  if (latestImages && stats.latestImagesReady) {
    divLatestImages.innerHTML = '';
    for (var i = 0; i < latestImages.length; i++) {
      var img = document.createElement('img');
      img.src = latestImages[i].thumb;
      img.loading = 'lazy';
      var link = document.createElement('a');
      var postPart = latestImages[i].postId ? '#' + latestImages[i].postId : '';
      link.href = '/' + latestImages[i].boardUri + '/res/' + latestImages[i].threadId + '.html' + postPart;
      link.appendChild(img);
      divLatestImages.appendChild(link);
    }
  }

  var latestPosts = receivedData.latestPosts;
  if (latestPosts && stats.latestPostsReady) {
    divLatestPosts.innerHTML = '';
    for (var i = 0; i < latestPosts.length; i++) {
      var latestPostCell = document.createElement('div');
      latestPostCell.className = 'latestPostCell';
      var linkPost = document.createElement('a');
      linkPost.className = 'linkPost title';
      var postPart = latestPosts[i].postId ? '#' + latestPosts[i].postId : '';
      linkPost.href = '/' + latestPosts[i].boardUri + '/res/' + latestPosts[i].threadId + '.html' + postPart;
      linkPost.innerText = '>>/' + latestPosts[i].boardUri + '/' + (latestPosts[i].postId ? latestPosts[i].postId : latestPosts[i].threadId);
      var br = document.createElement('br');
      var labelPreview = document.createElement('span');
      labelPreview.className = 'labelPreview';
      labelPreview.innerHTML = latestPosts[i].previewText;
      latestPostCell.appendChild(linkPost);
      latestPostCell.appendChild(br);
      latestPostCell.appendChild(labelPreview);
      divLatestPosts.appendChild(latestPostCell);
    }
  }

};

stats.refresh = function() {
  api.localRequest(stats.refreshURL, stats.refreshCallback);
};

stats.startTimer = function() {

  stats.refreshTimer = setInterval(function checkTimer() {

    stats.refresh();

  }, 5000);

};

stats.init();

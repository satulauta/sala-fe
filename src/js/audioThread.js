// ==UserScript==
// @name     KC audio thread
// @version  1
// @grant    none
// @match    https://kohlchan.net/*/res/*
// https://gitgud.io/Tjark/KohlNumbra/-/blob/master/src/js/audioThread.js
// ==/UserScript==


audioThread = {}

audioThread.audioFiles = ["mp3", "flac", "ogg", "opus"]

audioThread.audioPlayerLoaded = false;

audioThread.currentTrackNo = 0;
audioThread.playList = [];
audioThread.sourcePosts = {};

audioThread.init = function () {

  audioThread.audioPlayer = document.createElement("audio");
  document.body.appendChild(audioThread.audioPlayer);

  audioThread.playerDiv = document.createElement("div");
  audioThread.playerDiv.setAttribute('class', "playerBox");
  audioThread.playerDiv.classList.add("playerDivHidden");


  audioThread.playListDiv = document.createElement("div");
  audioThread.playListDiv.setAttribute('id', "playerList");

  audioThread.playerDiv.appendChild(audioThread.playListDiv);


  audioThread.controllersDiv = document.createElement("div");
  audioThread.controllersDiv.setAttribute("id", "playerControls");

  audioThread.playerDiv.appendChild(audioThread.controllersDiv);


  audioThread.autoPlayBoxLabel = document.createElement("label");
  audioThread.autoPlayBoxLabel.textContent = "Auto";
  audioThread.autoPlayBoxLabel.setAttribute('class', "brackets");

  audioThread.controllersDiv.appendChild(audioThread.autoPlayBoxLabel);


  audioThread.autoPlayBox = document.createElement("input");
  audioThread.autoPlayBox.setAttribute('type', 'checkbox');
  audioThread.autoPlayBox.setAttribute('checked', true);
  audioThread.autoPlayBox.setAttribute('id', "autoPlayAudioThreadCB");
  audioThread.autoPlayBoxLabel.prepend(audioThread.autoPlayBox);

  audioThread.shuflePlayListLabel = document.createElement("label");
  audioThread.shuflePlayListLabel.textContent = "Random";
  audioThread.shuflePlayListLabel.setAttribute('class', "brackets");

  audioThread.controllersDiv.appendChild(audioThread.shuflePlayListLabel);


  audioThread.autoPlayBox = document.createElement("input");
  audioThread.autoPlayBox.setAttribute('type', 'checkbox');
  audioThread.autoPlayBox.setAttribute('id', "randomPlayList");
  audioThread.shuflePlayListLabel.prepend(audioThread.autoPlayBox);

  audioThread.playButton = document.createElement("input");
  audioThread.playButton.setAttribute('type', 'button');
  audioThread.playButton.setAttribute('value', "▶ / ⏸︎");

  audioThread.controllersDiv.appendChild(audioThread.playButton);


  audioThread.prevButton = document.createElement("input");
  audioThread.prevButton.setAttribute('type', 'button');
  audioThread.prevButton.setAttribute('value', "⏮︎");

  audioThread.controllersDiv.appendChild(audioThread.prevButton);


  audioThread.nextButton = document.createElement("input");
  audioThread.nextButton.setAttribute('type', 'button');
  audioThread.nextButton.setAttribute('value', "⏭︎");

  audioThread.controllersDiv.appendChild(audioThread.nextButton);

  audioThread.volRange = document.createElement("input");
  audioThread.volRange.setAttribute('class', 'volControl');
  audioThread.volRange.setAttribute('type', 'range');
  audioThread.volRange.setAttribute('value', "100");
  audioThread.volRange.setAttribute('min', "0");
  audioThread.volRange.setAttribute('max', "100");

  audioThread.controllersDiv.appendChild(audioThread.volRange);


  audioThread.audioFilenameField = document.createElement("a");
  audioThread.audioFilenameField.textContent = "";
  audioThread.audioFilenameField.setAttribute("id", "audioFileName")
  audioThread.playerDiv.appendChild(audioThread.audioFilenameField);

  document.getElementById("postModerationFields").appendChild(audioThread.playerDiv);

  audioThread.audioListOpen = document.createElement("a")
  audioThread.audioListOpen.innerText = "Audio";
  audioThread.audioListOpen.classList.add("brackets");
  audioThread.audioListOpen.onclick = function toggleAudioView(e) {
    const classList = audioThread.playerDiv.classList;
    if (classList.contains("playerDivHidden")) {
      classList.remove("playerDivHidden")
    } else {
      classList.add("playerDivHidden");
    }
    return false;
  }

  audioThread.registerLinkClickListener();


  const divRefreshContainer = document.getElementById("divRefreshContainer");
  const divRefresh = document.getElementsByClassName("divRefresh")[0];
  divRefreshContainer.insertBefore(audioThread.audioListOpen, divRefresh);

  audioThread.initPlayer = function () {

    audioThread.audioPlayer.src = audioThread.playList[0];
    audioThread.currentTrackNo = 0;
    audioThread.audioPlayerLoaded = true;

  };


  audioThread.audioPlayer.addEventListener('ended', function () {
    var element = document.querySelectorAll('[data-trackno="' + audioThread.currentTrackNo + '"]');
    element[0].classList.remove("playing");

    var autoPlayCheckbox = document.getElementById('autoPlayAudioThreadCB').checked;
    if (autoPlayCheckbox) {
      audioThread.nextTitle();
    }
  });

  audioThread.audioPlayer.addEventListener('play', function () {

    if (audioThread.playerDiv.classList.contains("playerDivHidden")) {
      audioThread.playerDiv.classList.remove("playerDivHidden")
    }

    var allPlaying = document.querySelectorAll('.playButton.playing');
    for (i = 0; i < allPlaying.length; i++) {
      allPlaying[i].classList.remove("playing");
      allPlaying[i].setAttribute("value", "▶︎")
    }

    var nowPlaying = document.querySelectorAll('[data-trackno="' + audioThread.currentTrackNo + '"]');
    for (var i = 0; i < nowPlaying.length; i++) {
      nowPlaying[i].classList.add("playing");
      nowPlaying[i].setAttribute("value", "⏸")
    }

    const audioName =  audioThread.playList[audioThread.currentTrackNo].split("/").pop();
    audioThread.audioFilenameField.textContent = decodeURIComponent(audioName)
      .replaceAll("<", "&lt")
      .replaceAll(">", "&gt;");

    audioThread.audioFilenameField.href = "#" + audioThread.sourcePosts[audioThread.currentTrackNo];
  });

  audioThread.audioPlayer.addEventListener('pause', function () {
    var elements = document.querySelectorAll('[data-trackno="' + audioThread.currentTrackNo + '"]');
    if (elements.length > 0) {
      for (var i = 0; i < elements.length; i++) {
        elements[i].classList.remove("playing");
        elements[i].setAttribute("value", "▶︎")
      }
    }

  });

  audioThread.playButton.addEventListener("click", function () {
    audioThread.playTitle();
  });

  audioThread.prevButton.addEventListener("click", function () {
    audioThread.prevTitle();
  });

  audioThread.nextButton.addEventListener("click", function () {
    audioThread.nextTitle();
  });


  audioThread.volRange.oninput = function () {
    audioThread.audioPlayer.volume = this.value / 100;
  }

  audioThread.updateAllPosts();

};

audioThread.randomPlayListOrder = function() {
  return document.getElementById('randomPlayList').checked;
}

audioThread.scrollToPlayingIfNotHovered = function() {

  const isHovered = !!document.querySelector(".playerBox:hover");
  if (isHovered) {
    return;
  }

  const playingTrack = document.querySelector(".playing");
  if (!playingTrack) return;

  playingTrack.scrollIntoView({
    behavior: "smooth",
    block: "center"
  });
}

audioThread.getNewTrackNo = function(type) {
  switch(type) {
    case 'next':
      return audioThread.currentTrackNo + 1;
    case 'prev':
      return audioThread.currentTrackNo - 1;
    case 'random':
      return Math.floor(Math.random()*[...new Set(audioThread.playList)].length);
    default:
      throw new Error(`Invalid type '${type}' for next track.`)
  }
}

audioThread.playTitle = function () {
  if (audioThread.playList.length > 0) {
    if (!audioThread.audioPlayerLoaded) {
      audioThread.initPlayer();
    }
    if (audioThread.audioPlayer.paused) {
      audioThread.audioPlayer.play();
    } else {
      audioThread.audioPlayer.pause();
    }
  }
}


audioThread.prevTitle = function () {
  audioThread.audioPlayer.pause();

  const atPlayListStart = audioThread.currentTrackNo === 0;

  switch(audioThread.randomPlayListOrder()) {
    case true:
        audioThread.currentTrackNo = audioThread.getNewTrackNo("random");
        break;
    case false:
        audioThread.currentTrackNo = (atPlayListStart) ? audioThread.playList.length - 1
                                                       : audioThread.getNewTrackNo("prev");
        break;
  }

  setTimeout(() => {
    audioThread.audioPlayer.src = audioThread.playList[audioThread.currentTrackNo];
    audioThread.audioPlayer.play();
  })
}


audioThread.nextTitle = function () {
  audioThread.audioPlayer.pause();

  const atPlayListEnd = audioThread.currentTrackNo + 1 === audioThread.playList.length;

  switch(audioThread.randomPlayListOrder()) {
    case true:
        audioThread.currentTrackNo = audioThread.getNewTrackNo("random");
        break;
    case false:
        audioThread.currentTrackNo = (atPlayListEnd) ? 0 : audioThread.getNewTrackNo("next");
        break;
  }

  setTimeout(() => {
    audioThread.audioPlayer.src = audioThread.playList[audioThread.currentTrackNo];
    audioThread.audioPlayer.play();
  })
}


audioThread.updateAllPosts = function () {

  originalNameLinks = document.getElementsByClassName("nameLink");

  for (i = 0; i < originalNameLinks.length; i++) {
    audioThread.addAudioPlayer(originalNameLinks[i]);
  }
  audioThread.audioListOpen.innerText = `Audio (${audioThread.playList.length})`;
}

audioThread.registerLinkClickListener = function() {
  document.addEventListener("click", function(event) {
    const target = event.target;
    if (!target.classList.contains("playButton")) return;
    if (!audioThread.audioPlayer.paused && audioThread.currentTrackNo == event.srcElement.dataset.trackno) {
      audioThread.audioPlayer.pause();
    } else {
      audioThread.audioPlayer.pause();
      setTimeout(() => {
        audioThread.currentTrackNo = Number(event.srcElement.dataset.trackno);
        audioThread.audioPlayer.src = audioThread.playList[audioThread.currentTrackNo];
        audioThread.audioPlayer.play();
        audioThread.audioPlayerLoaded = true;
      })
    }
  });
}

audioThread.addAudioPlayer = function (nameLink) {

  currentFile = nameLink.href;

  const duplicateFile = !!audioThread.playList.find((it) => it === currentFile);
  console.log(duplicateFile);

  if (audioThread.audioFiles.includes(currentFile.split('.').pop())) {

    //var sourcePost = Number(nameLink.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.id);
    const post = (nameLink.closest(".postCell") || nameLink.closest(".opCell"));
    console.log(post);
    const sourcePost = Number(post.getAttribute("id"));
    console.log(sourcePost);
    var playListItem = document.createElement("div");
    playListItem.classList.add("playListItem")

    var audioName = document.createElement("a");
    var audioOriginalName = nameLink.getAttribute("download");
    if (audioOriginalName.length > 45) {
      const ext = currentFile.split('.').pop()
      audioOriginalName = audioOriginalName.substr(0, 41) + ".." + ext;
    }

    var addLink = document.createElement("input");
    addLink.setAttribute('type', 'button');
    addLink.setAttribute('value', "▶︎");
    addLink.classList.add("playButton");

    const trackNo = (duplicateFile) ? audioThread.playList.findIndex((it) => it === currentFile)
                                    : audioThread.playList.length;
                                    
    addLink.dataset.trackno = trackNo
    playListItem.appendChild(addLink)
    playListItem.appendChild(audioName);

    nameLink.parentNode.append(addLink.cloneNode(false))

    audioName.innerText = audioOriginalName;
    audioName.setAttribute("href", "#" + sourcePost);

    if (!duplicateFile) {
      audioThread.playListDiv.append(playListItem);

      audioThread.sourcePosts[addLink.dataset.trackno] = sourcePost;
      audioThread.playList.push(currentFile);
    }
  }
}

//if (settings.get('audioThread')) {
  audioThread.init();


window.addEventListener('load', function() {
  const postingAddPost = posting.addPost;
  posting.addPost = function(post, boardUri, threadId, noExtra, preview) {

    const postCell = postingAddPost(post, boardUri, threadId, noExtra, preview);
    const links = postCell.querySelectorAll(".nameLink");
    for (var i = 0; i < links.length; i++) {
      audioThread.addAudioPlayer(links[i])
    }
    audioThread.audioListOpen.innerText = `Audio (${audioThread.playList.length})`;

    return postCell;
  }
});

//}

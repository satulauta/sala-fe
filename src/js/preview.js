var preview = {}


preview.is_displayed = function() {
  return preview.container.style.display !== "hidden";
}

preview.init = function() {


  if (document.getElementById("catalogId")) return;

  preview.container = document.createElement('div');
  preview.container.id = 'previewContainer';
  preview.container.style.display = 'none';

  preview.img = document.createElement('img');
  preview.video = document.createElement('video');

  preview.video.autoload = true;
  preview.video.loop = true;
  preview.video.controls = true;

  preview.container.appendChild(preview.img);
  preview.container.appendChild(preview.video);

  document.body.appendChild(preview.container);

  if (!settings.get('centeredMediaDisplay')) {
    return;
  }

  var imageLinks = document.querySelectorAll('.imgLink, .linkThumb');

  posting.eventEmitter.addListener("postAdded", function postAdded(cell) {
    var imageLinks = cell.querySelectorAll('.imgLink'); 
    for (var i = 0; i < imageLinks.length; i++) {
      var link = imageLinks[i];
      link.onclick = preview.show;
    }  
  });

  for (var i = 0; i < imageLinks.length; i++) {
    var link = imageLinks[i];
    link.onclick = preview.show;
  }

  document.addEventListener("click", function(e) {
    console.log("häh")
    const target = e.target;
    if (target === preview.container || (target.parentNode && target.parentNode === preview.container)) {
      return preview.remove();
    }
    //return preview.remove()
  });

};

preview.show = function(event, imgLink = null) {

  preview.remove();

  if (imgLink) {
    console.log(imgLink);
    this.dataset = imgLink.dataset;
    this.dataset.filepath = imgLink.href
    console.log(this.dataset);
  }

  //if (this.getElementsByClassName('imgExpanded').length > 0) {
  //  return;
 // }

  var src = this.dataset.filepath ? this.dataset.filepath : this.href;
  var mime = this.dataset.filemime;

  if (mime === '') {
    return;
  }
  if (thumbs.playableTypes.indexOf(mime) > -1) {

    preview.img.style.display = 'none';
    preview.video.src = src;
    preview.video.style.display = 'inline-block';
    preview.video.play();
    preview.video.currentTime = 0;

  } else {

    preview.video.style.display = 'none';
    preview.img.src = src;
    preview.img.style.display = 'inline-block';

  }
  preview.container.style.display = 'block';
  return false;
};

preview.remove = function() {

  preview.img.style.display = 'none';
  preview.video.style.display = 'none';
  preview.video.pause();
  preview.video.currentTime = 0;
  preview.img.removeAttribute('src');
  preview.video.removeAttribute('src');
  preview.container.style.display = 'none';

  return false
};

preview.init();
